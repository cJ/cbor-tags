#######################################################
CBOR Extension - Multi-dimensional Arrays of Structures
#######################################################

This document registers a tag for Multidimensional arrays
(ND-arrays) in Concise Binary Object Representation (`CBOR`_):

:Tag: 279
:Data item: Map (major type 5) of integer keys to various.
:Semantics: Multi-dimensional Array of Structures.
:Revision: 0.1.0
:Author: Jérôme Carretero (cJ-cbor [@] zougloub [.] eu)


Any copyright to this specification is released to the
`Public Domain <https://creativecommons.org/publicdomain/zero/1.0>`_.


Introduction
############

There is already a CBOR extension for ND-arrays (:rfc:`8746`),
but its array items must be of scalar (aka simple arithmetic) types
(flavors of int or floats).

The extension defined here provides more support for
`Multi-dimensional Arrays`_,
in particular for those whose elements may be of compound types, which
in particular provides a means to fully represent `Numpy ND-arrays`_.


Multi-dimensional Arrays
************************

`Wikipedia on ND-arrays`_ provides the essence on how an ND-array is
represented and used.
`Numpy on ND-arrays`_ documents their implementation of such arrays.

An ND-array is stored contiguously in memory, and then there is a
mapping between coordinate indices and position of the item in memory.

In the simplest way, one can define the mapping from the array *shape*
and *index ordering*:

- The index ordering provides whether the first or last dimension
  of the index corresponds to neightboring memory locations.

  To illustrate:

  - Row-major indexing corresponds to `[...,i1,i0]` coordinates,
    where the array is represented in memory as
    `V[...,i1=0,i0=0],V[...,i1=0,i0=1],V[...,i1=0,i0=2]`.
    In this case the last index is the fastest varying one when
    scrolling through memory.

  - Column-major indexing corresponds to `[i0,i1,...]` coordinates,
    where the array is represented in memory as
    `V[i0=0,i1=0,...],V[i0=1,i1=0,...],V[i0=2,i1=0,...]`
    In this case the first index is the fastest varying one when
    scrolling through memory.

- The array shape is the extent of indices in each dimension.

  The order of dimensions in the shape depends on the index ordering.

  Considering a 2D array, a shape in row-major indexing is (#rows,
  #columns), while in column-major indexing it is (#columns, #rows).

As most programmers are more familiar with row-major indexing, the
default indexing is row-major.

In more advanced ways, the mapping also considers for each dimension a
minimum index and a stride; this is used to perform various kinds of
reindexing of ND-arrays that are memory-mapped.

Because the environment of the specification is data serialization,
we are only considering the simple representation.

ND-arrays may contain items which are not scalar, ie. that are
structures whose fields could be further accessed / indexed.

For example, a data table may be represented contiguously in memory,
with each row containing items of different types.


Detailed Semantics
##################

The ND-array is presented as map (major type 5), with the following items:

.. list-table::
   :header-rows: 1

   * - Key
     - Value

   * - -1
     - .. _`Untyped Array Values`:

       Untyped Array Values (Optional)

       The array values, as a simple CBOR array.

       If used and the `Data Type Descriptor`_ (key 1) is provided,
       then the values are to be converted to have an in-memory
       representation as specified by the descriptor.

       If used and the `Data Type Descriptor`_ (key 1) is not provided,
       then the values are to be deserialized and used as-is (as
       regular CBOR).

       Presence is mutually exclusive with that of Encoded Array Values.

   * - 0
     - .. _`Encoded Array Values`:

       Encoded Array Values (Optional)

       The array values, flattened, as a byte array.

       Presence is mutually exclusive with that of Encoded Array Values.

   * - 1
     - .. _`Data Type Descriptor`:

       Data type descriptor (optional)

       This can be:

       - Omitted, in which case the array is not a contiguous array,
         each element is of any type, and the Untyped Array Values
         must be used.

       - In the most common case, a type string, made of:

         - byte endianness:

           - "<" for little endian.

           - ">" for big endian.

           - "\|" for irrelevant (for byte-sized data).

         - item kind:

           They can be numpy ones:

           - "f" for float (used for binary16, binary32, binary64).
           - "c" for complex float (used for complex16, complex32, complex64).

           - "i" for signed int.
           - "u" for unsigned int.

           - "U" for UTF-32 code point string (note that in some rare
             cases, Python may be compiled to use UTF-16 ; such a
             configuration is unsupported).

           - "V" for structure (V as in void).

           - "O" is not supported (it contains pointers).

           Or extensions that may be used:

           - "r" for signed fraction (numerator and denominator are of
             equal size, half the fraction item size).

           - "R" for unsigned fraction.

         - Item size, in bytes.

         - Optional information (eg. for date-time and time delta).

         Examples:

         - "<f4": float32, in little endian.
         - ">i2": int16, in big endian.
         - ">r8": fraction of ">i4" and ">i4".

       - A tuple of (type descriptor, shape).

         This is for when each array element is itself an array.

       - A tuple, whose first element is a field name,
         and second element is a dictionary of (
         data type descriptor, offset, title)

       This data type descriptor representation is compatible with that of
       `Numpy Data type objects`_, allowing easy interoperability with it.

       Examples:

       - For an xRGB8888 pixel, we can have the following data type
         descriptor:

         ::

           ('|V4', {
            'r': ('|u1', 1),
            'g': ('|u1', 2),
            'b': ('|u1', 3)
            })

       - For items that would be defined in Numpy as:

         .. code:: python

            np.dtype([('name', np.unicode_, 16), ('grades', np.float32, (2,))])

         The descriptor is:

         ::

            ['|V32', {
             'name': ['<U16', 0],
             'grades': [['<f4', [2]], 64],
            }]

   * - 2
     - .. _`Index Ordering`:

       Index Ordering (optional)

       It can be:

       - 0 for row-major, 1 for column-major
       - Omitted, in which case this defaults to row-major

   * - 3
     - .. _`Shape`:

       Shape (optional)

       It can be:

       - An array of strictly postive values
       - A strictly positive value, which specifies the length of the
         array
       - Omitted, which means the array has a single dimension

   * - 4
     - .. _`Sparse`:

       Sparse Array Coordinates (optional)

       Can be used to define an array sparsely.

       This is an array of zero or more indices (or slices) where
       values are defined.

       If specified, then the Shape_ (key 4) must be specified.

       Indexes or slices are represented as follows:

       - A null value corresponds to all values (`[:]` in Python
         notation).

       - A null value indicates the whole set of values along this
         dimension.

         For example `[0,null,0]` corresponds to `[0,:,0]` in Python
         notation.

       - A tuple indicates a slice along this dimension.

         For example, `[[1,3],[3,5]]` will correspond to `[1:3,3:5]`
         in Python. The tuple values for the slice correspond to the
         first to the next-to-last index along the sliced dimension.


Arrays are zero-initialized by default: if no value is provided by
`Untyped Array Values`_  or `Encoded Array Values`_, then the array
must be zero-initialized.

If an array is not defined sparsely, then if the shape is defined, the
value array must match the expected number of elements (product of the
shape).


Examples
########


- Same example as
  https://www.rfc-editor.org/rfc/rfc8746.html#ex-multidim

  ::

     uint16_t a[2][3] = {
       {2, 4, 8},   /* row 0 */
       {4, 16, 256},
     };

  Gives (27 bytes):

  ::

     d9 01 17    # Tag 279: Multi-dimensional Array of Structures
       a3         # map(3)

         01        # 1 - key for Data Type Descriptor
         63        # UTF-8 string of 3
           3e 75 32 # ">i2": indicates big-endian int16

         03        # 3 - key for Shape
         82        # array(2)
           02       # shape is (2,3)
           03       #

         00        # 0 - key for Encoded Array Values
         4c        # byte string(12)
           00 02    # 2
           00 04    # 4
           00 08    # 8
           00 04    # 4
           00 10    # 16
           01 00    # 256

- Same example as
  https://www.rfc-editor.org/rfc/rfc8746.html#ex-multidim1

  ::

     uint16_t a[2][3] = {
       {2, 4, 8},   /* row 0 */
       {4, 16, 256},
     };

  Gives (23 bytes):

  ::

     d9 01 17    # Tag 279: Multi-dimensional Array of Structures
       a3         # map(3)

         01        # 1 - key for Data Type Descriptor
         63        # UTF-8 string of 3
           3e 75 32 # ">i2": indicates big-endian int16

         03        # 3 - key for Shape
         82        # array(2)
           02       # shape is (2,3)
           03       #

         20        # -1 - key for Untyped Array Values
         86        # array(6)
           02       # 2
           04       # 4
           08       # 8
           04       # 4
           10       # 16
           19 01 00 # 256

- Zero array:

  ::

     uint16_t a[2][3] = {
      {0, 0, 0},
      {0, 0, 0},
     };


  Gives (13 bytes):

  ::

     d9 01 17    # Tag 279: Multi-dimensional Array of Structures
       a3         # map(3)

         01        # 1 - key for Data Type Descriptor
         63        # UTF-8 string of 3
           3e 75 32 # ">i2": indicates big-endian int16

         03        # 3 - key for Shape
         82        # array(2)
           02       # shape is (2,3)
           03       #

- Sparse array:

  ::

     uint16_t a[2][4] = {
      {2, 1, 2, 2},
      {2, 1, 2, 2},
     };

  Gives (23 bytes):

  ::

     d9 01 17    # Tag 279: Multi-dimensional Array of Structures
       a3         # map(3)

         01        # 1 - key for Data Type Descriptor
         63        # UTF-8 string of 3
           3e 75 32 # ">i2": indicates big-endian int16

         03        # 3 - key for Shape
         82        # array(2)
           02       # shape is (2,4)
           04       #

         04        # 4 - key for Sparse Array Coordinates
         82        # array(2) - 2 values
          22        # null - [:] - slice whole array to set default value
          82        # array(2) - 2D index
           22 01     # [null, 1] - [:,1] - slice second column

         20        # -1 - key for Untyped Array Values
         82        # array(2)
           02       # 2
           01       # 1

- Numpy example:

  This:

  .. code:: python

     dt = np.dtype([('name', np.unicode_, 6), ('grades', np.float32, (2,))])
     x = np.array([('Sarah', (8.0, 7.0)), ('John', (6.0, 7.0))], dtype=dt)

  Gives (110 bytes):

  ::

     d9 01 17
       a3

         01 # key 1 - data type descriptor
         82 # ['|V32', {'name': ['<U6', 0], 'grades': [['<f4', [2]], 24]}]
           64
             7c563332 # "|V32"
           a2
             64
               6e616d65 # "name"
             82
               63
                 3c5536 # "<U6"
               00 # 0

             66
               677261646573 # grades
             82
               82
                 63
                   3c6634 # "<f4"
                 81
                   02 # 2
                 18 18 # 24

         03 # key 3 - shape
         81
           02 # (2,)

         00 # key 0 - data
         58 40
          53000000 61000000 72000000 61000000 68000000 00000000 00000041 0000e040
          #      S        a        r        a        h      NUL      8.0      7.0

          4a000000 6f000000 68000000 6e000000 00000000 00000000 0000c040 0000e040
          #      J        o        h        n      NUL      NUL      6.0      7.0




Considerations
##############


Interoperability Considerations
*******************************

This extension was mostly designed in order to provide a simple CBOR-based
serialization format for Numpy arrays.

Construction of arrays using the sparse fashion doesn't need to be
actually generating sparse arrays.

Construction of masked arrays is not supported at this point, although
additional features are possible because the top-level data item is a
mapping.

There are also additional features that can be used in some contexts.


Support of this Extension
*************************

An implementation supporting this specification partially or fully
shall give an error when encoding mapping keys that are not known,
as their presence could alter the values of the reconstructed
array.

This specification doesn't define how an implementation ought to
behave if it doesn't support data items covered by this extension.
See `how the CBOR specification details the kind of behaviour
that can be used
<https://www.rfc-editor.org/rfc/rfc8949.html#section-3.4-6>`_.



Security Considerations
***********************

Usual CBOR security considerations apply ; refer to the `CBOR
Specification <CBOR_>`_ section on `Security Considerations
<https://www.rfc-editor.org/rfc/rfc8949.html#name-security-considerations>`_.

It is possible to define arrays of ridiculously large shape
without actually transmitting them, thanks to the sparse feature;
this may lead to denial-of-service attacks if not handled.

When constructiong ND-arrays from their serialization, implementations
must be careful of not performing out-of-bounds accesses, by checking
the compatibility of data.
When constructing an array from a sparse description, the length of
the array must match the length of the values ; the length of the
indice tuples must match the number of dimensions and be within the
array shape.

When using encoded character strings (for example if a more compact
representation than the provided UTF-32 support offers), please
consider the potential security issues when converting to Unicode due
to misinterpretation of the encoded bytes (see § 3.6 of
`Unicode Technical Report 36 on Security Considerations`_).


References
##########

- .. _CBOR:

  Bormann, C. and Hoffman, P. "Concise Binary Object Representation
  (CBOR)". :rfc:`7049`, October 2013.

- .. _`Wikipedia on ND-arrays`:

  `Wikipedia on Contiguous Multidimensional Arrays
  <https://en.wikipedia.org/wiki/Array_(data_structure)#Multidimensional_arrays>`_

- .. _`Numpy on ND-arrays`:
  .. _`Numpy ND-arrays`:

  `Numpy Documentation - The N-dimensional array (ndarray)
  <https://numpy.org/doc/stable/reference/arrays.ndarray.html>`_

- .. _`Numpy Data type objects`:

  `Numpy Documentation - Data type objects <https://numpy.org/doc/stable/reference/arrays.dtypes.html>`_.

- .. _`Unicode Technical Report 36 on Security Considerations`:

  `Davis, M. and Suignard, M. "Unicode Security
  Considerations". Unicode Technical Report 36, 12 Nov. 2013.
  <http://unicode.org/reports/tr36/>`_
